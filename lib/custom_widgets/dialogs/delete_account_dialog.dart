// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:tajen/custom_widgets/buttons/custom_button.dart';
// import 'package:tajen/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
// import 'package:tajen/custom_widgets/custom_text_form_field/validation_mixin.dart';
// import 'package:tajen/networking/api_provider.dart';
// import 'package:tajen/providers/auth_provider.dart';
// import 'package:tajen/providers/cancel_account_provider.dart';
// import 'package:tajen/utils/app_colors.dart';
// import 'package:tajen/utils/commons.dart';
// import 'package:tajen/utils/urls.dart';



// class DeleteAccountDialog extends StatefulWidget {


//   @override
//   _DeleteAccountDialogState createState() => _DeleteAccountDialogState();
// }

// class _DeleteAccountDialogState extends State<DeleteAccountDialog> with ValidationMixin {
//    final _formKey = GlobalKey<FormState>();
//    double _width = 0;
//    ApiProvider _apiProvider = ApiProvider();
//    String _deleteReason = '';
//    AuthProvider _authProvider;
//    CancelAccountProvider _cancelAccountProvider;

   
//  @override
//   Widget build(BuildContext context) {
//    _authProvider = Provider.of<AuthProvider>(context);
//    _cancelAccountProvider = Provider.of<CancelAccountProvider>(context);
//    _width = MediaQuery.of(context).size.width;

//     return AlertDialog(
//       contentPadding: EdgeInsets.fromLTRB(10.0 ,20.0,10.0,20.0),
//       shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.all(Radius.circular(10.0))),
//       content: SingleChildScrollView(
//         child: Form(
//           key: _formKey,
//           child:
        
//              Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//               Text(
//                       'طلب حذف حسابي',
                     
//                       style: TextStyle(fontSize: 16,
//                       fontWeight: FontWeight.w700, color:
//                       mainAppColor ),
//                     ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Text('سبب الحذف',
//                  style: TextStyle(fontSize: 16,
//                       fontWeight: FontWeight.w500, color:
//                       Color(0xff212529) ),
//                     )
//                 // Divider(
//                 //   height: 2,
//                 //   color: Color(0xff707070),
//                 // )
//                 ,
//                   SizedBox(
//                   height: 5,
//                 ),
//                 CustomTextFormField(
                
//                  maxLines: 3,
//                   enableHorizontalMargin: false,
//                   hintTxt: 'اكتب للإدارة أسباب طلب الحذف',
//                   validationFunc: validateDeleteReason,
//                   onChangedFunc: (String text)=>
//                     _deleteReason = text
                  
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),

//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Container(
//                       height: 60,
//                       width: _width *0.35,
//                       child: CustomButton(
//                         horizontalMarginIsEnabled: false,
//                         btnColor:mainAppColor,
//                         btnLbl: 'إرسال الطلب',
//                          btnStyle: TextStyle(
//                           color: Colors.white,
//                           fontSize: 14,
//                           fontWeight: FontWeight.bold
//                         ),
//                         onPressedFunction: () async {
//                           if(_formKey.currentState.validate()){
//                       _cancelAccountProvider.setIsLoading(true);
//                       final results =
//                           await _apiProvider.post(Urls.DELETE_REQUEST_URL, body: {
//                         "delete_reason": _deleteReason
                       
                     
//                       } ,headers:   {"Accept-Language" :"ar",
//           "authorize" : _authProvider.accessToken,
//                           "Accept" : "application/json"});

//                      _cancelAccountProvider.setIsLoading(false);

//                       if (results['status'] == 200) 
//                       {
//                           Commons.showToast(context,message: results['message']);
//                           Navigator.pop(context);
                       
                       
                          
//                       }
                      
//                        else 
//                         Commons.showError(context, results['message']);
//                           }
//                         },
//                       ),
//                     ),
//                       Container(
//                            height: 60,
//                       width: _width *0.35,
//                       child: CustomButton(
//                         horizontalMarginIsEnabled: false,
//                         btnColor:Color(0xffE9ECEF),
//                         btnStyle: TextStyle(
//                           color: Color(0xff333B42),
//                           fontSize: 12,
//                           fontWeight: FontWeight.bold
//                         ),
//                         btnLbl: 'إلغاء',
//                         onPressedFunction: ()=> Navigator.pop(context),
//                       ),
//                     )
//                   ],
//                 )
//                 // Container(
//                 //   height: 70,
//                 //   child: CustomButton(
//                 //     btnLbl: 'تأكيد ',
//                 //     onPressedFunction: (){
//                 //       // if(_formKey.currentState.validate())_cancelOrder();
//                 //     },
//                 //   ),
//                 // )
            
//               ],
//             ),
//           // ),
//         ),
//       ),
//     );
//   }
// }
