import 'package:flutter/material.dart';

import '../../utils/app_colors.dart';



class LogoutDialog extends StatelessWidget {
  const LogoutDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              title: Text(
                "هل تريد تسجيل الخروج؟",
                style: TextStyle( color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
              ),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('إلغاء',
                  style:  TextStyle(
                          color: mainAppColor,
                          fontSize: 14,
                          fontWeight: FontWeight.bold
                        ),),
                ),
                FlatButton(
                      onPressed: () {
Navigator.pop(context);
  Navigator.of(context).pushNamedAndRemoveUntil(
         '/login_screen', (Route<dynamic> route) => false);
                      },
                      child: Text('موافق',
                         style:  TextStyle(
                          color: mainAppColor,
                          fontSize: 14,
                          fontWeight: FontWeight.bold
                        ),),
                    )
              ],
            );
  }
}