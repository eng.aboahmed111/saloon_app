import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerRestaurant extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Shimmer.fromColors(
                baseColor: Color(0xffF8F7F9),
                highlightColor: Colors.grey[350],
                child
              : Container(
             
                  decoration: BoxDecoration(
                      color: Color(0xffF8F7F9),
              borderRadius: BorderRadius.circular(15.0)),
          
                 margin: EdgeInsets.symmetric(horizontal: width *0.02,
                     vertical: 10),
              width: width,
                      height: 200,
              
              
              )
    
              );
  }
}
