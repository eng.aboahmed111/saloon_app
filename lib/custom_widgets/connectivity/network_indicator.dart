import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';

import '../../locale/app_localizations.dart';


class NetworkIndicator extends StatefulWidget {
  final Widget child;
  const NetworkIndicator({this.child});
  @override
  _NetworkIndicatorState createState() => _NetworkIndicatorState();
}

class _NetworkIndicatorState extends State<NetworkIndicator> {
  double _width = 0;

  Widget _buildBodyItem() {
    return ListView(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 100,
            ),
            Icon(
              Icons.signal_wifi_off,
              size: 120,
              color: Colors.grey[400],
            ),
            Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  AppLocalizations.of(context)
                      .translate('sorry..no_internet_connection'),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
                )),
            Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  AppLocalizations.of(context).translate('check_your_router'),
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[400],
                      fontWeight: FontWeight.w400),
                )),
            Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  AppLocalizations.of(context)
                      .translate('reconnect_to_network'),
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[400],
                      fontWeight: FontWeight.w400),
                )),
            Container(
                height: 50,
                margin: EdgeInsets.symmetric(
                    horizontal: _width * 0.25, vertical: 10),
                child: Builder(
                    builder: (context) => ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            elevation: 500,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(25.0)),
                            primary: Theme.of(context).primaryColor,
                          ),
                          child: Container(
                              alignment: Alignment.center,
                              child: new Text(
                                AppLocalizations.of(context)
                                    .translate('refresh_screen'),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20.0),
                              )),
                        )))
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return OfflineBuilder(
      connectivityBuilder: (
        BuildContext context,
        ConnectivityResult connectivity,
        Widget child,
      ) {
        if (connectivity == ConnectivityResult.none) {
          final appBar = AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text(
              'Tajan',
              style: TextStyle(
                  color: Color(0xffFFFFFF),
                  fontSize: 20,
                  fontWeight: FontWeight.w400),
            ),
            centerTitle: true,
          );
          _width = MediaQuery.of(context).size.width;

          return Scaffold(
            appBar: appBar,
            body: _buildBodyItem(),
          );
        } else
          return child;
      },
      builder: (BuildContext context) {
        return widget.child;
      },
    );
  }
}
