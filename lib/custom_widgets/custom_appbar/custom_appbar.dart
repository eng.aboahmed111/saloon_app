import 'package:flutter/material.dart';

import '../../utils/app_colors.dart';


class CustomAppBar extends PreferredSize {
  final Widget child;
  final double height;
  final bool backBtnIsEnabled;
  final Widget titleWidget;
  final Widget leadingWidget;
  final Widget trailingWidget;
  final Function backButtonAction;

  CustomAppBar(
      {this.backBtnIsEnabled: true,
      this.titleWidget,
      this.leadingWidget,
      this.trailingWidget,
      this.backButtonAction,
      this.child,
      this.height = kToolbarHeight});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: preferredSize.height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
        color: mainAppColor
      
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          leadingWidget != null
              ? leadingWidget
              : backBtnIsEnabled
                  ? IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: 15,
                      ),
                      onPressed: backButtonAction == null
                          ? () => Navigator.pop(context)
                          : backButtonAction)
                  : Container(),
          Spacer(
            flex: 3,
          ),
         titleWidget,
          Spacer(
            flex: 3,
          ),
          trailingWidget != null
              ? trailingWidget
              : Container(
                  width: 20,
                ),
        ],
      ),
    );
  }
}
