import 'package:flutter/material.dart';

import '../../locale/app_localizations.dart';


mixin ValidationMixin<T extends StatefulWidget> on State<T> {
  String _password = '', _newPassword = '';

  String validatePhoneOrUserName(String value) {
    if (value.trim().length == 0) return 'يرجى إدخال الهاتف أو  اسم المستخدم';

    return null;
  }

  String validateUserEmail(String userEmail) {
    if (userEmail.trim().length == 0)
      return AppLocalizations.of(context).translate('email_validation');

    return null;
  }

  String validateUserPhone(String phoneNo) {
    if (phoneNo.trim().length == 0) return 'يرجى إدخال رقم الموبيل بشكل صحيح ';
    return null;
  }

  String validateUserName(String userName) {
    if (userName.trim().length == 0) return 'يرجى إدخال اسم المستخدم';

    return null;
  }

  String validateSectionNo(String sectioNo) {
    if (sectioNo.trim().length == 0) return 'يرجى إدخال رقم القطعة';

    return null;
  }

  String validateStreetName(String streetName) {
    if (streetName.trim().length == 0) return 'يرجى إدخال اسم الشارع';

    return null;
  }

  String validateBuildingNo(String buildingNo) {
    if (buildingNo.trim().length == 0) return ' يرجى إدخال  رقم المبنى';

    return null;
  }

  String validateAvenueName(String avenueName) {
    if (avenueName.trim().length == 0) return ' يرجى إدخال اسم الجادة';

    return null;
  }

  String validateFloorNo(String avenueName) {
    if (avenueName.trim().length == 0) return ' يرجى إدخال رقم الطابق';

    return null;
  }

  String validateFlatNo(String flatNo) {
    if (flatNo.trim().length == 0) return ' يرجى إدخال رقم الشقة';

    return null;
  }

  String validateFullName(String fullName) {
    if (fullName.trim().length == 0) return 'يرجى إدخال اسمك كاملاً';
    return null;
  }

  String validatePhoneNo(String phoneNo) {
    if (phoneNo.trim().length == 0) return 'يرجى إدخال رقم الهاتف';
    return null;
  }

  String validatePhoneKey(String phoneKey) {
    if (phoneKey.trim().length == 0) return 'يرجى إدخال المفتاح';
    return null;
  }

  String validatePassword(String password) {
    _password = password;
    if (password.trim().length == 0) return 'يرجى إدخال كلمة المرور الحالية';

    return null;
  }

  String validateConfirmPassword(String confirmPassword) {
    if (confirmPassword.trim().length == 0)
      return AppLocalizations.of(context)
          .translate('confirm_password_validation');
    else if (_password != confirmPassword)
      return AppLocalizations.of(context).translate('Password_not_identical');

    return null;
  }

  String validateCurrentPassword(String password) {
    if (password.trim().length == 0) return 'يرجى إدخال كلمة المرور الحالية';

    return null;
  }

  String validateNewPassword(String newPassword) {
    _newPassword = newPassword;
    if (_newPassword.trim().length == 0) {
      return 'يرجى إدخال كلمة المرور الجديدة';
    }
    return null;
  }

  String validateConfirmNewPassword(String confirmPassword) {
    if (confirmPassword.trim().length == 0)
      return 'يرجى إدخال تأكيد كلمة المرور الجديدة';
    else if (_newPassword != confirmPassword)
      return 'كلمة المرور الجديدة غير متطابقة';

    return null;
  }

  // String validateKeySearch(String keySearch) {
  //   if (keySearch.trim().length == 0) {
  //     return ' يرجى إدخال كلمة البحث';
  //   }
  //   return null;
  // }
  String validateDeleteReason(String deleteReason) {
    if (deleteReason.trim().length == 0) return 'يرجى إدخال سبب الحذف';

    return null;
  }

  String validateOrderComment(String refusReason) {
    if (refusReason.trim().length == 0) return 'يرجى إدخال تعليقك';

    return null;
  }

  String validateAccountHolderName(String accountHolderName) {
    if (accountHolderName.trim().length == 0) {
      return AppLocalizations.of(context)
          .translate('account_holder_Name_validation');
    }
    return null;
  }

  String validateAccountNumber(String accountNumber) {
    if (accountNumber.trim().length == 0)
      return AppLocalizations.of(context)
          .translate('account_number_validation');

    return null;
  }

  String validateCardNo(String cardNo) {
    if (cardNo.trim().length == 0)
      return AppLocalizations.of(context).translate('card_no_validation');

    return null;
  }

  String validateExpiryMonth(String expiryMonth) {
    if (expiryMonth.trim().length != 2)
      return AppLocalizations.of(context).translate('expiry_month_validation');

    return null;
  }

  String validateExpiryYear(String expiryYear) {
    if (expiryYear.trim().length != 4)
      return AppLocalizations.of(context).translate('expiry_year_validation');

    return null;
  }

  String validateNameOfEat(String name) {
    if (name.trim().length == 0) return '  يرجى إدخال اسم الصنف  ';

    return null;
  }

  String validateDescription(String name) {
    if (name.trim().length == 0) return '  يرجى إدخال الوصف  ';

    return null;
  }

  String validateIngredients(String ingredients) {
    if (ingredients.trim().length == 0) return '  يرجى إدخال المكونات  ';

    return null;
  }

  String validateUnitMeasure(String unitMeasure) {
    if (unitMeasure.trim().length == 0) return '  يرجى إدخال وحدة القياس  ';

    return null;
  }

  String validateMinOrder(String minOrder) {
    if (minOrder.trim().length == 0)
      return ' يرجى إدخال الحد الأدنى للطلب';
    else if (double.parse(minOrder) <= 0)
      return 'يرجى إدخال الحد الأدنى للطلب  بشكل صحيح';

    return null;
  }

  String validateAmountOfOrder(String amount) {
    if (amount.trim().length == 0)
      return ' يرجى إدخال أعلى كمية  للطلب';
    else if (double.parse(amount) <= 0)
      return ' يرجى إدخال أعلى كمية  للطلب بشكل صحيح';

    return null;
  }

  String validatePrice(String price) {
    if (price.trim().length == 0)
      return ' يرجى إدخال السعر ';
    else if (double.parse(price) <= 0) return 'يرجى إدخال السعر بشكل صحيح';

    return null;
  }

//   bool checkValidationOfCardImg(AuthProvider authProvider) {
//     if (authProvider.cardImg != null)
//       authProvider.setCardImgError(false);
//     else
//       authProvider.setCardImgError(true);

//     if (authProvider.cardImg != null)
//       return true;
//     else
//       return false;
//   }

//   bool checkValidationOfAddingAddress(
//       {AuthProvider authProvider,
//       Address selectedCountry,
//       Address selectedGovernorate,
//       Address selectedRegion}) {
//     if (selectedCountry != null)
//       authProvider.setCountryError(false);
//     else
//       authProvider.setCountryError(true);

//     if (selectedGovernorate != null)
//       authProvider.setGovernorateError(false);
//     else
//       authProvider.setGovernorateError(true);

//     if (selectedRegion != null)
//       authProvider.setRegionError(false);
//     else
//       authProvider.setRegionError(true);

//     if (selectedCountry != null &&
//         selectedGovernorate != null &&
//         selectedRegion != null)
//       return true;
//     else
//       return false;
//   }

//    bool checkValidationOfFoodMenuSettings(
//       {@required FoodMenuSettingsProvider foodMenuSettingsProvider,
//      }) {
//     if (foodMenuSettingsProvider.fromTime != null)
//       foodMenuSettingsProvider.setFromTimeValidation(false);
//     else
//       foodMenuSettingsProvider.setFromTimeValidation(true);

// if (foodMenuSettingsProvider.toTime != null)
//       foodMenuSettingsProvider.setToTimeValidation(false);
//     else
//       foodMenuSettingsProvider.setToTimeValidation(true);

// if (foodMenuSettingsProvider.orderDateCancel != null)
//       foodMenuSettingsProvider.setOrderDateCancelValidation(false);
//     else
//       foodMenuSettingsProvider.setOrderDateCancelValidation(true);

// if (foodMenuSettingsProvider.orderCancelTime != null)
//       foodMenuSettingsProvider.setOrderCancelTimeValidation(false);
//     else
//       foodMenuSettingsProvider.setOrderCancelTimeValidation(true);

//     if (foodMenuSettingsProvider.fromTime != null &&
//   foodMenuSettingsProvider.toTime != null&&
//         foodMenuSettingsProvider.orderDateCancel != null &&
//         foodMenuSettingsProvider.orderCancelTime != null
//         )
//       return true;
//     else
//       return false;
//   }

//   bool checkValidationOfAddingMenuItem(
//       {AddingMenuItemProvider addingMenuItemProvider,
//       }) {
//     if (addingMenuItemProvider.mainImg != null)
//       addingMenuItemProvider.setMainImgValidation(false);
//     else
//       addingMenuItemProvider.setMainImgValidation(true);

//     if (addingMenuItemProvider.mainImg != null)
//       return true;
//     else
//       return false;
//   }

//    bool checkValidationOfTotalOrdersToSpecificPeriod(
//       {TotalOrdersProvider totalOrdersProvider,
//       }) {
//     if (totalOrdersProvider.dateFrom != null)
//       totalOrdersProvider.setDateFromValidation(false);
//     else
//       totalOrdersProvider.setDateFromValidation(true);
//  if (totalOrdersProvider.dateTo != null)
//       totalOrdersProvider.setDateToValidation(false);
//     else
//       totalOrdersProvider.setDateToValidation(true);

//     if (totalOrdersProvider.dateFrom != null &&totalOrdersProvider.dateTo != null )
//       return true;
//     else
//       return false;
//   }
}
