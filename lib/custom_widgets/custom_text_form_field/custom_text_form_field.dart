import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../../utils/app_colors.dart';


class CustomTextFormField extends StatefulWidget {
  final TextStyle errorStyle;
  final double radius;
  final double horizontalMargin;
  final bool defaultHorizontalMarginIsEnabled;
  final bool enabled;
  final String initialValue;
  final String hintTxt;
  final TextInputType inputData;
  final bool isPassword;
  final Function onSavedFunction;
  final Function validationFunction;
  final Function onChangedFunction;
  final Function onTapFunction;
  final Widget suffix;
  final Widget suffixIcon;
  final bool suffixIconIsImage;
  final String suffixIconImagePath;
  final int maxLength;
  final int maxLines;
  final Widget prefix;
  final Widget prefixIcon;
  final bool prefixIconIsImage;
  final String prefixIconImagePath;
  final String labelTxt;
  final bool expands;
  final double verticalPadding;
  final double horizontalPadding;
  final TextEditingController controller;
  final Color unfocusColor;
  final Color hintColor;
  final Color focusColor;
  final bool filled;
  final List<MaskTextInputFormatter> inputFormatters;

  CustomTextFormField(
      {this.hintTxt,
      this.errorStyle,
      this.radius,
      this.horizontalMargin,
      this.defaultHorizontalMarginIsEnabled: true,
      this.inputData,
      this.isPassword: false,
      this.onSavedFunction,
      this.validationFunction,
      this.onChangedFunction,
      this.onTapFunction,
      this.initialValue,
      this.suffixIcon,
      this.maxLength,
      this.prefixIconIsImage: false,
      this.suffixIconIsImage: false,
      this.prefixIconImagePath,
      this.suffixIconImagePath,
      this.enabled: true,
      this.maxLines = 1,
      this.expands = false,
      this.labelTxt,
      this.prefix,
      this.verticalPadding,
      this.horizontalPadding,
      this.unfocusColor,
      this.hintColor,
      this.focusColor,
      this.suffix,
      this.filled: true,
      this.prefixIcon,
      this.controller,
      this.inputFormatters});

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool _obsecureText = true;
  FocusNode _focusNode;

  @override
  void initState() {
    _focusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    _focusNode.dispose();

    super.dispose();
  }

  Widget _buildTextFormField() {
    return TextFormField(
      onSaved: widget.onSavedFunction,
      onTap: widget.onTapFunction,
      inputFormatters: widget.inputFormatters,
      expands: widget.expands,
      controller: widget.controller,
      enabled: widget.enabled,
      maxLines: widget.maxLines,
      focusNode: _focusNode,
      maxLength: widget.maxLength,
      initialValue: widget.initialValue,
      style: TextStyle(
          color: Colors.black, fontSize: 15, fontWeight: FontWeight.w400),
      decoration: InputDecoration(
        filled: widget.filled ? true : false,
        fillColor: Color(0xffFFFFFF),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
              widget.radius == null ? 10.0 : widget.radius),
          borderSide: BorderSide(
              color: _focusNode.hasFocus
                  ? widget.focusColor != null
                      ? widget.focusColor
                      : mainAppColor
                  : widget.unfocusColor != null
                      ? widget.unfocusColor
                      : hintColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
              widget.radius == null ? 10.0 : widget.radius),
          borderSide: BorderSide(
              color: _focusNode.hasFocus
                  ? widget.focusColor != null
                      ? widget.focusColor
                      : mainAppColor
                  : widget.unfocusColor != null
                      ? widget.unfocusColor
                      : hintColor),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: widget.horizontalPadding == null
                ? 12.0
                : widget.horizontalPadding,
            vertical:
                widget.verticalPadding == null ? 12.0 : widget.verticalPadding),
        suffix: widget.isPassword
            ? InkWell(
                onTap: () {
                  setState(() {
                    _obsecureText = !_obsecureText;
                  });
                },
                child: Icon(
                  _obsecureText ? Icons.remove_red_eye : Icons.visibility_off,
                  color: _focusNode.hasFocus ? mainAppColor : hintColor,
                  size: 20,
                ),
              )
            : SizedBox(
                height: 20,
              ),
        suffixIcon: !widget.suffixIconIsImage
            ? widget.suffixIcon
            : _focusNode.hasFocus
                ? Image.asset(
                    widget.suffixIconImagePath,
                    color: mainAppColor,
                    height: 25,
                    width: 25,
                  )
                : Image.asset(
                    widget.suffixIconImagePath,
                    color: Colors.grey,
                    height: 25,
                    width: 25,
                  ),
        prefix: widget.prefix,
        prefixIcon: !widget.prefixIconIsImage
            ? widget.prefixIcon
            : _focusNode.hasFocus
                ? Image.asset(
                    widget.prefixIconImagePath,
                    color: mainAppColor,
                    height: 25,
                    width: 25,
                  )
                : Image.asset(
                    widget.prefixIconImagePath,
                    color: hintColor,
                    height: 25,
                    width: 25,
                  ),
        hintText: widget.hintTxt,
        labelText: widget.labelTxt,
        labelStyle: TextStyle(
            color: Color(0xff0D0D0F).withOpacity(0.5),
            fontSize: 12,
            fontWeight: FontWeight.bold),
        errorStyle: widget.errorStyle != null
            ? widget.errorStyle
            : TextStyle(fontSize: 12.0),
        hintStyle: TextStyle(
            color: _focusNode.hasFocus
                ? mainAppColor
                : widget.hintColor != null
                    ? widget.hintColor
                    : Color(0xff7F2C2D80).withOpacity(0.5),
            fontSize: 14,
            fontWeight: FontWeight.w400),
      ),
      keyboardType: widget.inputData,
      obscureText: widget.isPassword ? _obsecureText : false,
      validator: widget.validationFunction,
      onChanged: widget.onChangedFunction,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(
            horizontal: widget.horizontalMargin != null
                ? widget.horizontalMargin
                : widget.defaultHorizontalMarginIsEnabled
                    ? 20.0
                    : 0),
        child: _buildTextFormField());
  }
}
