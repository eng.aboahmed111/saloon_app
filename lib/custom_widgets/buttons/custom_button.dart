import 'package:flutter/material.dart';
import 'package:saloon/utils/app_colors.dart';


class CustomButton extends StatelessWidget {
  final bool defaultHorizontalMarginIsEnabled;
  final bool defaultVerticalMarginIsEnabled;
  final Color btnColor;
  final Color borderColor;
  final double height;
  final double horizontalMargin;
  final double verticalMargin;
  final double borderRadius;
  final String btnLbl;
  final Function onPressedFunction;
  final TextStyle btnLblStyle;
  final Widget prefixIcon;
  final Widget postfixIcon;
  final bool gradientColorIsEnabled;

  const CustomButton(
      {Key key,
      this.btnLbl,
      this.borderColor,
      this.height,
      this.borderRadius,
      this.verticalMargin,
      this.horizontalMargin,
      this.defaultHorizontalMarginIsEnabled: true,
      this.defaultVerticalMarginIsEnabled: true,
      this.onPressedFunction,
      this.btnColor,
      this.btnLblStyle,
      this.prefixIcon,
      this.gradientColorIsEnabled = false,
      this.postfixIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onPressedFunction(),
      child: Container(
          height: height != null ? height :50.0,
          margin: EdgeInsets.symmetric(
              horizontal: horizontalMargin != null
                  ? horizontalMargin
                  : defaultHorizontalMarginIsEnabled
                      ? 20.0
                      : 0.0,
              vertical: verticalMargin != null
                  ? verticalMargin
                  : defaultVerticalMarginIsEnabled
                      ? 10.0
                      : 0),
          decoration: gradientColorIsEnabled
              ? BoxDecoration(
                  borderRadius: BorderRadius.circular(18.0),
                  gradient: LinearGradient(
                    colors: [
                      Color(0xffFE585A),
                      Color(0xffFDB970),
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                  boxShadow: [
                      BoxShadow(
                        color: Colors.grey[500],
                        offset: Offset(0.0, 1.5),
                        blurRadius: 1.5,
                      ),
                    ])
              : BoxDecoration(
                  color: btnColor != null ? btnColor : mainAppColor,
                  border: Border.all(
                    width: 1.0,
                    color: borderColor != null
                        ? borderColor
                        : btnColor != null
                            ? btnColor
                            : mainAppColor,
                  ),
                  borderRadius: BorderRadius.circular(
                      borderRadius != null ? borderRadius : 30)),
          alignment: Alignment.center,
          child: prefixIcon != null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    prefixIcon,
                    Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        child: Text(
                          '$btnLbl',
                          textAlign: TextAlign.center,
                          style: btnLblStyle == null
                              ? Theme.of(context).textTheme.button
                              : btnLblStyle,
                        ))
                  ],
                )
              : postfixIcon != null
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            child: Text(
                              '$btnLbl',
                              textAlign: TextAlign.center,
                              style: btnLblStyle == null
                                  ? Theme.of(context).textTheme.button
                                  : btnLblStyle,
                            )),
                        postfixIcon
                      ],
                    )
                  : Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      child: Text(
                        '$btnLbl',
                        textAlign: TextAlign.center,
                        style: btnLblStyle == null
                            ? Theme.of(context).textTheme.button
                            : btnLblStyle,
                      ))),
    );
  }
}
