import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../../utils/app_colors.dart';

class CustomDropdownButtonFormField extends StatefulWidget {
  final List<DropdownMenuItem<dynamic>> items;
  final String hint;
  final dynamic value;
  final Function onChangeFunc;
  final TextStyle errorStyle;
  final double radius;
  final bool horizontalMarginIsEnabled;
  final bool enabled;
  final String initialValue;
  final String hintTxt;
  final TextInputType inputData;
  final Function validationFunc;
  final Function onChangedFunc;
  final Widget suffix;
  final Widget suffixIcon;
  final bool suffixIconIsImage;
  final String suffixIconImagePath;

  final Widget prefix;
  final Widget prefixIcon;
  final bool prefixIconIsImage;
  final String prefixIconImagePath;
  final String labelTxt;
  final bool isExpanded;
  final double verticalPadding;
  final double horizontalPadding;
  final TextEditingController controller;
  final Color unfocusColor;
  final Color hintColor;
  final Color focusColor;
  final bool filled;
  final List<MaskTextInputFormatter> inputFormatters;

  const CustomDropdownButtonFormField(
      {Key key,
      this.items,
      this.hint,
      this.value,
      this.onChangeFunc,
      this.errorStyle,
      this.radius,
      this.horizontalMarginIsEnabled: true,
      this.enabled,
      this.initialValue,
      this.hintTxt,
      this.inputData,
      this.validationFunc,
      this.onChangedFunc,
      this.suffix,
      this.suffixIcon,
      this.suffixIconIsImage: false,
      this.suffixIconImagePath,
      this.prefix,
      this.prefixIcon,
      this.prefixIconIsImage: false,
      this.prefixIconImagePath,
      this.labelTxt,
      this.isExpanded: true,
      this.verticalPadding,
      this.horizontalPadding,
      this.controller,
      this.unfocusColor,
      this.hintColor,
      this.focusColor,
      this.filled: true,
      this.inputFormatters})
      : super(key: key);
  @override
  _CustomDropdownButtonFormFieldState createState() =>
      _CustomDropdownButtonFormFieldState();
}

class _CustomDropdownButtonFormFieldState
    extends State<CustomDropdownButtonFormField> {
  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    _focusNode.dispose();

    super.dispose();
  }

  Widget _buildDropdownButtonFormField() {
    return DropdownButtonFormField(
      style: TextStyle(
          fontFamily: 'Fairuz',
          color: Colors.black,
          fontSize: 15,
          fontWeight: FontWeight.w400),
      value: widget.value,
      isExpanded: widget.isExpanded,
      decoration: InputDecoration(
        filled: widget.filled ? true : false,
        fillColor: Color(0xffFFFFFF),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
              widget.radius == null ? 10.0 : widget.radius),
          borderSide: BorderSide(
              color: _focusNode.hasFocus
                  ? widget.focusColor != null
                      ? widget.focusColor
                      : mainAppColor
                  : widget.unfocusColor != null
                      ? widget.unfocusColor
                      : hintColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
              widget.radius == null ? 10.0 : widget.radius),
          borderSide: BorderSide(
              color: _focusNode.hasFocus
                  ? widget.focusColor != null
                      ? widget.focusColor
                      : mainAppColor
                  : widget.unfocusColor != null
                      ? widget.unfocusColor
                      : hintColor),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: widget.horizontalPadding == null
                ? 12.0
                : widget.horizontalPadding,
            vertical:
                widget.verticalPadding == null ? 12.0 : widget.verticalPadding),
        suffix: widget.suffix,
        suffixIcon: !widget.suffixIconIsImage
            ? widget.suffixIcon
            : _focusNode.hasFocus
                ? Image.asset(
                    widget.suffixIconImagePath,
                    color: mainAppColor,
                    height: 25,
                    width: 25,
                  )
                : Image.asset(
                    widget.suffixIconImagePath,
                    color: Colors.grey,
                    height: 25,
                    width: 25,
                  ),
        prefix: widget.prefix,
        prefixIcon: !widget.prefixIconIsImage
            ? widget.prefixIcon
            : _focusNode.hasFocus
                ? Image.asset(
                    widget.prefixIconImagePath,
                    color: mainAppColor,
                    height: 25,
                    width: 25,
                  )
                : Image.asset(
                    widget.prefixIconImagePath,
                    color: hintColor,
                    height: 25,
                    width: 25,
                  ),
        hintText: widget.hintTxt,
        labelText: widget.labelTxt,
        labelStyle: TextStyle(
            color: Color(0xff0D0D0F).withOpacity(0.5),
            fontSize: 12,
            fontWeight: FontWeight.bold),
        errorStyle: widget.errorStyle != null
            ? widget.errorStyle
            : TextStyle(fontSize: 12.0),
        hintStyle: TextStyle(
            color: _focusNode.hasFocus
                ? mainAppColor
                : widget.hintColor != null
                    ? widget.hintColor
                    : Color(0xff7F2C2D80).withOpacity(0.5),
            fontSize: 14,
            fontWeight: FontWeight.w400),
      ),
      onChanged: widget.onChangeFunc,
      items: widget.items,
    );
    //  Container(
    //        height: 55,
    //     padding: const EdgeInsets.all(8.0),
    //     margin: widget.elementHasDefaultMargin
    //         ? EdgeInsets.symmetric(
    //             horizontal: MediaQuery.of(context).size.width * 0.07)
    //         : EdgeInsets.symmetric(horizontal: 0),
    //     decoration: BoxDecoration(
    //       color: Color(0xffF8F9FA),
    //       borderRadius: BorderRadius.circular(10.0),
    //       border: Border.all(color: hintColor),
    //     ),
    //     child: DropdownButtonHideUnderline(
    //       child: DropdownButton<dynamic>(
    //         isExpanded: true,
    //         hint: Text(
    //           widget.hint,
    //           style: TextStyle(
    //               color: hintColor,
    //               fontSize: 14,
    //               fontWeight: FontWeight.w400,
    //               fontFamily: 'Tajawal'
    //               ),
    //         ),
    //         focusColor: mainAppColor,
    //         icon: Icon(
    //                Icons.keyboard_arrow_down,
    //                size: 20,
    //                color: Color(0xff495057),
    //              ),
    //         style: TextStyle(
    //             fontSize: 14,
    //             color: Colors.black,
    //             fontWeight: FontWeight.w400,
    //             fontFamily: 'Tajawal'),
    //         items: widget.dropDownList,
    //         onChanged: widget.onChangeFunc,
    //         value: widget.value,
    //       ),
    //     ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(
            horizontal: widget.horizontalMarginIsEnabled ? 20 : 0),
        child: _buildDropdownButtonFormField());
  }
}
