import 'package:flutter/material.dart';

class OnboardingProvider extends ChangeNotifier {
  int _currentPageValue = 0;


  void upadateCurrentPageValue(int value ){
    _currentPageValue = value;
    notifyListeners();
  }

  int get currentPageValue => _currentPageValue;
}