import 'package:flutter/material.dart';

import '../networking/api_provider.dart';

class AuthProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();

  // current language from shared prefernces 'ar' or 'en'
  String _currentLang;

  void setCurrentLanguage(String currentLang) {
    _currentLang = currentLang;
    notifyListeners();
  }

  String get currentLang => _currentLang;

  // User _currentUser;

  // void setCurrentUser(User currentUser) {
  //   _currentUser = currentUser;
  //   notifyListeners();
  // }

  // User get currentUser => _currentUser;

  // String _userPhone;

  // void setUserPhone(String userPhone) {
  //   _userPhone = userPhone;
  //   notifyListeners();
  // }

  // String get userPhone => _userPhone;

  // bool _isSelectedTerms = false;

  // void setIsSelected(bool value) {
  //   _isSelectedTerms = value;
  //   notifyListeners();
  // }

  // bool get isSelectedTerms => _isSelectedTerms;

  // String _accessToken;

  // void setAccessToken(String token) {
  //   _accessToken = token;
  //   notifyListeners();
  // }

  // String get accessToken => _accessToken;

  // String _fullName;

  // void setFullName(String fullName) {
  //   _fullName = fullName;
  //   notifyListeners();
  // }

  // String get fullName => _fullName;

  // String _phoneKey;

  // void setPhoneKey(String phoneKey) {
  //   _phoneKey = phoneKey;
  //   notifyListeners();
  // }

  // String get phoneKey => _phoneKey;

  // String _userName;

  // void setUserName(String userName) {
  //   _userName = userName;
  //   notifyListeners();
  // }

  // String get userName => _userName;

  // String _userPassword;

  // void setUserPassword(String password) {
  //   _userPassword = password;
  //   notifyListeners();
  // }

  // String get userPassword => _userPassword;

  // bool _chefAccountIsEnable = true;

  // void setChefAccountIsEnable(bool value) {
  //   _chefAccountIsEnable = value;
  //   notifyListeners();
  // }

  // bool get chefAccountIsEnable => _chefAccountIsEnable;

  // File _cardImg;

  // void setCardImg(File image, {bool notifyListener = true}) {
  //   _cardImg = image;
  //   if (notifyListener) notifyListeners();
  // }

  // File get cardImg => _cardImg;

  // bool _cardImageError = false;

  // void setCardImgError(bool value, {bool notifyListener = true}) {
  //   _cardImageError = value;
  //   if (notifyListener) notifyListeners();
  // }

  // bool get cardImageError => _cardImageError;

  //   bool _countryError = false;

  // void setCountryError(bool value, {bool notifyListener = true}) {
  //   _countryError = value;
  //   if (notifyListener) notifyListeners();
  // }

  // bool get countryError => _countryError;

  // bool _governorateError = false;

  // void setGovernorateError(bool value, {bool notifyListener = true}) {
  //   _governorateError = value;
  //   if (notifyListener) notifyListeners();
  // }

  // bool get governorateError => _governorateError;

  // bool _regionError = false;

  // void setRegionError(bool value, {bool notifyListener = true}) {
  //   _regionError = value;
  //   if (notifyListener) notifyListeners();
  // }

  // bool get regionError => _regionError;

  // List<Asset> _userAttachments = List<Asset>();

  // void setUserAttachments(List<Asset> attachments,
  //     {bool notifyListener = true}) {
  //   _userAttachments = attachments;
  //   if (notifyListener) notifyListeners();
  // }

  // List<Asset> get userAttachments => _userAttachments;

  // //pick imgs
  // Future<void> loadAssets(BuildContext context) async {
  //   List<Asset> resultList = List<Asset>();
  //   try {
  //     resultList = await MultiImagePicker.pickImages(
  //       maxImages: 30,
  //       enableCamera: true,
  //       // selectedAssets: userAttachments,
  //       cupertinoOptions: CupertinoOptions(takePhotoIcon: "attachments"),
  //       materialOptions: MaterialOptions(
  //         actionBarColor: "#3F72DC",
  //         actionBarTitle: "select photos",
  //         allViewTitle: "All Photos",
  //         useDetailsView: false,
  //         statusBarColor: "#3F72DC",
  //         selectCircleStrokeColor: "#000000",
  //       ),
  //     );
  //   } on Exception catch (e) {
  //     print(e.toString());
  //   }

  //   if (resultList.length > 5)
  //     Commons.showToast(
  //       context,
  //       message: '* 5 يرجى اختيار صور فرعية لاتزيد عن  '.toString(),
  //     );
  //   else
  //     setUserAttachments(resultList);
  // }

  // void removeItemFromAssets(int index) {
  //   _userAttachments.removeAt(index);
  //   notifyListeners();
  // }

  // Future<List<Address>> getCountryList() async {
  //   final response = await _apiProvider.get(Urls.COUNTRIES_URL);
  //   List<Address> countryList = List<Address>();
  //   if (response['status'] == 200) {
  //     Iterable iterable = response['data'];
  //     countryList = iterable.map((model) => Address.fromJson(model)).toList();
  //   }

  //   return countryList;
  // }

  // Future<List<Address>> getCityList(int countryId) async {
  //   final response =
  //       await _apiProvider.get(Urls.CITIES_URL + countryId.toString());
  //   List<Address> cityList = List<Address>();
  //   if (response['status'] == 200) {
  //     Iterable iterable = response['data'];
  //     cityList = iterable.map((model) => Address.fromJson(model)).toList();
  //   }

  //   return cityList;
  // }

  // Future<List<Address>> getRegionList(int cityId) async {
  //   final response =
  //       await _apiProvider.get(Urls.REGIONS_URL + cityId.toString());
  //   List<Address> regionList = List<Address>();
  //   if (response['status'] == 200) {
  //     Iterable iterable = response['data'];
  //     regionList = iterable.map((model) => Address.fromJson(model)).toList();
  //   }

  //   return regionList;
  // }

  //  Future<List<MultipartFile>> convertAssets(List<Asset> imgs) async {
  //      List<MultipartFile> multipartImageList = new List<MultipartFile>();
  //         if (null != imgs) {
  //           for (Asset asset in imgs) {
  //             ByteData byteData = await asset.getByteData();
  //             List<int> imageData = byteData.buffer.asUint8List();
  //             MultipartFile multipartFile = new MultipartFile.fromBytes(
  //               imageData,
  //               filename:  asset.identifier,
  //           //    contentType: MediaType("image", "jpg"),
  //             );
  //             multipartImageList.add(multipartFile);
  //           }

  //      }
  //       return multipartImageList;
  //   }
}
