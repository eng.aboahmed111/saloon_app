import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utils/app_colors.dart';


ThemeData materialThemeData() {
  return ThemeData(
      primaryColor: mainAppColor,
      hintColor: hintColor,
      brightness: Brightness.light,
      buttonColor: mainAppColor,
      accentColor: accentColor,
      scaffoldBackgroundColor: Color(0xffFFFFFF),
      fontFamily: 'Fairuz',
      platform: TargetPlatform.iOS,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: mainAppColor,
        // selectionColor: mainAppColor,
        // selectionHandleColor: Colors.blue,
      ),
      textTheme: TextTheme(

          // app bar style
          headline1: TextStyle(
              color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
          headline2: TextStyle(
              color: mainAppColor, fontSize: 20, fontWeight: FontWeight.w500),

          // hint style of text form
          headline3: TextStyle(
              color: hintColor, fontSize: 14, fontWeight: FontWeight.w400),
          button: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),

          // title
          headline4: TextStyle(
              color: Color(0xff343A40),
              fontWeight: FontWeight.bold,
              fontSize: 16.0),

          // subtitle
          headline5: TextStyle(
              color: Color(0xff6C757D),
              fontWeight: FontWeight.bold,
              fontSize: 14.0),
//title in table
          headline6: TextStyle(
              color: Color(0xff343A40),
              fontWeight: FontWeight.bold,
              fontSize: 14.0)));
}
