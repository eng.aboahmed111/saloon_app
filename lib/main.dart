import 'package:device_preview/device_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'locale/app_localizations.dart';
import 'locale/locale_helper.dart';
import 'providers/auth_provider.dart';
import 'shared_preferences/shared_preferences_helper.dart';
import 'theme/style.dart';
import 'utils/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) {
    run();
  });
}

void run() async => runApp(MyApp());
// runApp(
//       DevicePreview(
//         enabled: !kReleaseMode,
//         builder: (context) => MyApp(), // Wrap your app
//       ),
//     );
//

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;
  onLocaleChange(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  Future<void> _getLanguage() async {
    String language = await SharedPreferencesHelper.getString('lang') ?? 'ar';
    onLocaleChange(Locale(language));
  }

  @override
  void initState() {
    helper.onLocaleChanged = onLocaleChange;
    _locale = new Locale('ar');
    _getLanguage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => AuthProvider(),
          ),

          // ChangeNotifierProvider(
          //   create: (_) => NavigationProvider(),
          // ),

//  ChangeNotifierProxyProvider<AuthProvider, DayFoodMenuProvider>(
//             create: (_) => DayFoodMenuProvider(),
//             update: (_, auth, dayFoodMenuProvider) =>
//                 dayFoodMenuProvider..update(auth),
//           ),
        ],
        child: MaterialApp(
          title: 'Super Heros',
          locale: _locale,
          builder: DevicePreview.appBuilder,
          supportedLocales: [
            Locale('en', 'US'),
            Locale('ar', ''),
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate
          ],
          routes: routes,
          theme: materialThemeData(),
          debugShowCheckedModeBanner: false,
        ));
  }
}
