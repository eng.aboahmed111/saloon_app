// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:nurseryapp/custom_widgets/connectivity/network_indicator.dart';
// import 'package:nurseryapp/custom_widgets/safe_area/page_container.dart';
// import 'package:nurseryapp/locale/app_localizations.dart';
// import 'package:nurseryapp/providers/navigation_provider.dart';
// import 'package:nurseryapp/utils/app_colors.dart';
// import 'package:provider/provider.dart';

// class BottomNavigation extends StatefulWidget {
//   @override
//   _BottomNavigationState createState() => _BottomNavigationState();
// }

// class _BottomNavigationState extends State<BottomNavigation> {
//   bool _initialRun = true;

// // NotificationProvider _notificationProvider;
// //   FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
// //   FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
// //       new FlutterLocalNotificationsPlugin();

//   // void _iOSPermission() {

//   //   _firebaseMessaging.requestNotificationPermissions(
//   //       IosNotificationSettings(sound: true, badge: true, alert: true));
//   //   _firebaseMessaging.onIosSettingsRegistered
//   //       .listen((IosNotificationSettings settings) {
//   //     print("Settings registered: $settings");
//   //   });
//   // }

// //   void _firebaseCloudMessagingListeners() {
// //     var android = new AndroidInitializationSettings('mipmap/ic_launcher');
// //     var ios = new IOSInitializationSettings();
// //     var platform = new InitializationSettings(android, ios);
// //     _flutterLocalNotificationsPlugin.initialize(platform);
// //     // _firebaseMessaging.onTokenRefresh.listen((newToken) async {
// //     //   print('newToken: $newToken');
// //     //   await _services.post(Utils.UPDATE_FCM_TOKEN_URL, header: {
// //     //     'Accept': 'application/json',
// //     //     'Content-Type': 'application/json',
// //     //     'Authorization': 'Bearer ${_appState.currentUser.token}'
// //     //   }, body: {
// //     //     'mobile_token': newToken
// //     //   });
// //     // });
// //     if (Platform.isIOS) _iOSPermission();
// //     _firebaseMessaging.configure(
// //       onMessage: (Map<String, dynamic> message) async {
// //         print('on message $message');
// //         _showNotification(message);
// //         _notificationProvider.getUnReadNotificationsCount();

// //       },
// //       onResume: (Map<String, dynamic> message) async {
// //         print('on resume $message');
// //  _notificationProvider.getUnReadNotificationsCount();
// //         Navigator.pushNamed(context, '/notification_screen');
// //       },
// //       onLaunch: (Map<String, dynamic> message) async {
// //         print('on launch $message');
// //  _notificationProvider.getUnReadNotificationsCount();
// //         Navigator.pushNamed(context, '/notification_screen');
// //       },
// //     );
// //   }

// //   _showNotification(Map<String, dynamic> message) async {
// //     var android = new AndroidNotificationDetails(
// //       'channel id',
// //       "CHANNLE NAME",
// //       "channelDescription",
// //     );
// //     var iOS = new IOSNotificationDetails();
// //     var platform = new NotificationDetails(android, iOS);
// //     await _flutterLocalNotificationsPlugin.show(
// //         0,
// //         message['notification']['title'],
// //         message['notification']['body'],
// //         platform);
// //   }

//   // @override
//   // void didChangeDependencies() {
//   //   super.didChangeDependencies();
//   //   if (_initialRun) {
//   //      _notificationProvider = Provider.of<NotificationProvider>(context);
//   //     _firebaseCloudMessagingListeners();
//   //      _initialRun = false;
//   //   }
//   // }
//   @override
//   Widget build(BuildContext context) {
//     return NetworkIndicator(child: Consumer<NavigationProvider>(
//         builder: (context, navigationProvider, child) {
//       return PageContainer(
//         child: Scaffold(
//           body: navigationProvider.selectedContent,
//           bottomNavigationBar: BottomNavigationBar(
//             items: <BottomNavigationBarItem>[
//               BottomNavigationBarItem(
//                   icon: Icon(
//                     Icons.home,
//                     color: Color(0xff0D0D0F),
//                   ),
//                   activeIcon: Icon(Icons.home, color: accentColor),
//                   label: AppLocalizations.of(context).translate('home')),
//               BottomNavigationBarItem(
//                 icon: Icon(
//                   Icons.image_outlined,
//                   color: Color(0xff0D0D0F),
//                 ),
//                 label: AppLocalizations.of(context).translate('photo_gallery'),
//                 activeIcon: Icon(Icons.image_outlined, color: accentColor),
//               ),
//               BottomNavigationBarItem(
//                   icon: Icon(
//                     Icons.person_outline_outlined,
//                     color: Color(0xff0D0D0F),
//                   ),
//                   activeIcon:
//                       Icon(Icons.person_outline_outlined, color: accentColor),
//                   label: AppLocalizations.of(context).translate('profile')),
//               BottomNavigationBarItem(
//                 icon: Icon(
//                   Icons.more_horiz_outlined,
//                   color: Color(0xff0D0D0F),
//                 ),
//                 label: AppLocalizations.of(context).translate('more'),
//                 activeIcon: Icon(Icons.more_horiz_outlined, color: accentColor),
//               ),
//             ],
//             currentIndex: navigationProvider.navigationIndex,
//             selectedItemColor: accentColor,
//             unselectedItemColor: Color(0xFF495057),
//             onTap: (int index) {
//               navigationProvider.upadateNavigationIndex(index);
//               // if ((index == 1 || index == 2 || index == 3) &&
//               //     _appState.currentUser == null) {
//               //   Navigator.pushNamed(context, '/login_screen');
//               // } else {
//               //   _navigationState.upadateNavigationIndex(index);
//               // }
//             },
//             elevation: 5,
//             backgroundColor: Colors.white,
//             type: BottomNavigationBarType.shifting,
//           ),
//         ),
//       );
//     }));
//   }
// }
