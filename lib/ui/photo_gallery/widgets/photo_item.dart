// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';

// class PhotoItem extends StatelessWidget {
//   final AnimationController animationController;
//   final Animation animation;

//   const PhotoItem({Key key, this.animationController, this.animation})
//       : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return AnimatedBuilder(
//         animation: animationController,
//         builder: (BuildContext context, Widget child) {
//           return FadeTransition(
//               opacity: animation,
//               child: new Transform(
//                   transform: new Matrix4.translationValues(
//                       0.0, 50 * (1.0 - animation.value), 0.0),
//                   child: Container(
//                     margin: EdgeInsets.symmetric(horizontal: 1.0.w),
//                     child: ClipRRect(
//                         borderRadius: BorderRadius.circular(19.0),
//                         child: Image.asset('assets/images/children.png')),
//                   )));
//         });
//   }
// }
