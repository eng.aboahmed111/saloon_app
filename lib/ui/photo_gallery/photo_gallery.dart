// import 'package:flutter/material.dart';
// import 'package:nurseryapp/custom_widgets/custom_appbar/custom_appbar.dart';
// import 'package:nurseryapp/locale/app_localizations.dart';
// import 'package:sizer/sizer.dart';

// import 'widgets/photo_item.dart';

// class PhotoGalleryScreen extends StatefulWidget {
//   @override
//   _PhotoGalleryScreenState createState() => _PhotoGalleryScreenState();
// }

// class _PhotoGalleryScreenState extends State<PhotoGalleryScreen>
//     with TickerProviderStateMixin {
//   AnimationController _animationController;

//   @override
//   void initState() {
//     _animationController = AnimationController(
//         duration: Duration(milliseconds: 2000), vsync: this);
//     super.initState();
//   }

//   @override
//   void dispose() {
//     _animationController.dispose();
//     super.dispose();
//   }

//   Widget _buildBodyItem() {
//     return Container(
//         margin: EdgeInsets.symmetric(horizontal: 2.0.w),
//         child: GridView.builder(
//             itemCount: 10,
//             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//               crossAxisCount: 2,
//               childAspectRatio: 1.0,
//             ),
//             itemBuilder: (BuildContext context, int index) {
//               var count = 10;
//               var animation = Tween(begin: 0.0, end: 1.0).animate(
//                 CurvedAnimation(
//                   parent: _animationController,
//                   curve: Interval((1 / count) * index, 1.0,
//                       curve: Curves.fastOutSlowIn),
//                 ),
//               );
//               _animationController.forward();

//               return PhotoItem(
//                 animation: animation,
//                 animationController: _animationController,
//               );
//             }));
//   }

//   @override
//   Widget build(BuildContext context) {
//     final appBar = CustomAppBar(
//       height: 90,
//       leadingWidget: IconButton(
//         icon: Icon(
//           Icons.notifications_none_outlined,
//           color: Colors.white,
//         ),
//         onPressed: () {},
//       ),
//       titleWidget: Text(
//         AppLocalizations.of(context).translate('photo_gallery'),
//         style: Theme.of(context).textTheme.headline1,
//       ),
//       trailingWidget: IconButton(
//         icon: Icon(
//           Icons.search,
//           color: Colors.white,
//         ),
//         onPressed: () {},
//       ),
//     );
//     return Scaffold(
//       appBar: appBar,
//       body: _buildBodyItem(),
//     );
//   }
// }
