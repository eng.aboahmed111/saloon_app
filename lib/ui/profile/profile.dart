// import 'package:flutter/material.dart';
// import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
// import 'package:nurseryapp/locale/app_localizations.dart';
// import 'package:nurseryapp/utils/app_colors.dart';
// import 'package:sizer/sizer.dart';

// class ProfileScreen extends StatefulWidget {
//   @override
//   _ProfileScreenState createState() => _ProfileScreenState();
// }

// class _ProfileScreenState extends State<ProfileScreen> {
//   Widget _buildBodyItem() {
//     return Column(children: [
//       Stack(children: [
//         ClipPath(
//           clipper: OvalBottomBorderClipper(),
//           child: Container(
//             height: 25.0.h,
//             color: mainAppColor,
//           ),
//         ),
//         Column(
//           children: [
//             Container(
//               height: 2.0.h,
//             ),
//             Container(
//               margin: EdgeInsets.symmetric(horizontal: 5.0.w),
//               height: 30.0.h,
//               decoration: BoxDecoration(
//                   color: accentColor,
//                   borderRadius: BorderRadius.circular(20.0)),
//             ),
//           ],
//         ),
//         Column(children: [
//           Container(
//             height: 5.0.h,
//           ),
//           CircleAvatar(
//             radius: 5.5.h,
//             backgroundImage: NetworkImage("https://i.imgur.com/BoN9kdC.png"),
//           ),
//           Text(
//             AppLocalizations.of(context).translate('mother_or_father_name'),
//             style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 12.0.sp,
//                 fontWeight: FontWeight.w600),
//           ),
//           Text(
//             '010254652155',
//             style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 10.0.sp,
//                 fontWeight: FontWeight.w600),
//           )
//         ]),
//         Positioned(
//             height: 7.0.h,
//             bottom: 0,
//             left: 0.0.w,
//             right: 0.0.w,
//             child: Container(
//               alignment: Alignment.center,
//               width: 100.0.w,
//               margin: EdgeInsets.symmetric(horizontal: 5.0.w),
//               decoration: BoxDecoration(
//                   color: Colors.black.withOpacity(0.05),
//                   borderRadius: BorderRadius.circular(20.0)),
//               child: Text(
//                 'تعديل الملف الشخصي',
//                 style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 10.0.sp,
//                     fontWeight: FontWeight.w600),
//               ),
//             ))
//       ])
//     ]);
//   }

//   @override
//   Widget build(BuildContext context) {
//     final appBar = AppBar(
//       elevation: 0,
//       leading: IconButton(
//         icon: Icon(Icons.notifications_none_outlined),
//         color: Colors.white,
//         onPressed: () {},
//       ),
//       title: Text(
//         AppLocalizations.of(context).translate('profile'),
//         style: Theme.of(context).textTheme.headline1,
//       ),
//       actions: [IconButton(icon: Icon(Icons.search), onPressed: () {})],
//       centerTitle: true,
//     );
//     return Scaffold(
//       appBar: appBar,
//       body: _buildBodyItem(),
//     );
//   }
// }
