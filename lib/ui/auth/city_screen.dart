import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:saloon/ui/auth/screen_login.dart';

import '../../custom_widgets/custom_drop_down/custom_drop_down.dart';

class CityScreen extends StatefulWidget {
  @override
  _CityScreenState createState() => _CityScreenState();
}

class _CityScreenState extends State<CityScreen> {
  List<String> items = ['مصر', 'السعودية', 'الامارات', 'قطر'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          margin: EdgeInsets.all(20.0),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(18.0)),
          child: Form(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    margin: EdgeInsets.only(left: 89, right: 89),
                    child: Image.asset('assets/images/2.png')),
                SizedBox(
                  height: 42,
                ),
                CustomDropdownButtonFormField(
                  hintTxt: 'اختر الدولة',
                  items: items.map(
                        (val) {
                      return DropdownMenuItem<String>(
                        value: val,
                        child: Text(val),
                      );
                    },
                  ).toList(),
                  onChangeFunc: (val) {
                    setState(
                          () {
                        val = val;
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ScreenLogin()),
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
