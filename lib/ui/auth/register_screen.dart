import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:saloon/ui/auth/code_validation.dart';
import 'package:saloon/ui/auth/screen_login.dart';

import '../../custom_widgets/buttons/custom_button.dart';
import '../../custom_widgets/custom_text_form_field/custom_text_form_field.dart';

class ScreenRegister extends StatefulWidget {
  @override
  _ScreenRegisterState createState() => _ScreenRegisterState();
}

class _ScreenRegisterState extends State<ScreenRegister> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Container(
            height: 440,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.center,
                    colors: [Color(0xffE72E56), Color(0xff801D37)])),
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 110, right: 25, left: 25),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(18) ,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),

              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        top: 66, right: 106, left: 106, bottom: 10),
                    child: Image.asset('assets/images/home1.png'),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      // ignore: deprecated_member_use
                      FlatButton(
                        height: 35,
                        minWidth: 95,
                        child: Text('تسجيل جديد'),
                        shape: Border(
                          bottom: BorderSide(color: Colors.pink, width: 3),
                        ),
                        onPressed: () {
                          print('You selected all posts');
                        },
                      ),

                      // ignore: deprecated_member_use
                      FlatButton(
                        height: 35,
                        minWidth: 95,
                        child: Text('تسجيل دخول'),
                        shape: Border(
                            bottom: BorderSide(color: Colors.white, width: 3)),
                        onPressed: () {
                          // ignore: deprecated_member_use
                          FlatButton(
                            shape: Border(
                                bottom:
                                    BorderSide(color: Colors.pink, width: 3)),
                            onPressed: () {},
                            child: null,
                          );

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScreenLogin()),
                          );
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 28,
                  ),
                  CustomTextFormField(
                    hintTxt: 'اسم المستخدم',
                    prefixIcon: Icon(Icons.account_circle_outlined),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  CustomTextFormField(
                    hintTxt: 'البريد الإلكتروني',
                    prefixIcon: Icon(Icons.email),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  CustomTextFormField(
                    hintTxt: 'رقم الجوال',
                    prefixIcon: Icon(Icons.phone),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  CustomTextFormField(
                    hintTxt: 'كلمة السر',
                    prefixIcon: Icon(Icons.security),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  CustomTextFormField(
                    hintTxt: 'اعادة كلمة السر',
                    prefixIcon: Icon(Icons.security),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('الجنس'),
                      SizedBox(
                        width: 110,
                      ),
                      Row(
                        children: [
                          // Icon(Icons.check_box_outline_blank),
                          Checkbox(
                            activeColor: Colors.grey,
                            value: true,
                            onChanged: (bool value) {
                              setState(() {
                                if (value == true) {
                                  value = false;
                                }
                              });
                            },
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text('ذكر'),
                          SizedBox(
                            width: 15,
                          ),
                          // Icon(Icons.check_box_outline_blank)  ,
                          Checkbox(
                            activeColor: Colors.grey,
                            value: false,
                            onChanged: (bool value) {
                              setState(() {
                                if (value == false) value = true;
                              });
                            },
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text('أنثي')
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  CustomButton(
                    btnColor: Colors.pink,
                    btnLbl: 'تسجيل',
                    height: 50,
                    onPressedFunction: () {
                      setState(() {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CodeVerification()),
                        );
                      });
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    height: 36,
                    minWidth: 26,
                    child: Text(
                      'تخطي',
                      style: TextStyle(
                        fontSize: 13,
                      ),
                    ),
                    shape: Border(
                        bottom: BorderSide(color: Colors.white, width: 0)),
                    onPressed: () {
                      print('You selected all posts');
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
