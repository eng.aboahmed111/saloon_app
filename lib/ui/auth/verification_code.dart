// import 'dart:async';
//
// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:nurseryapp/custom_widgets/background_image/background_image.dart';
// import 'package:nurseryapp/custom_widgets/buttons/custom_button.dart';
// import 'package:nurseryapp/custom_widgets/connectivity/network_indicator.dart';
// import 'package:nurseryapp/custom_widgets/safe_area/page_container.dart';
// import 'package:nurseryapp/locale/app_localizations.dart';
// import 'package:nurseryapp/providers/auth_provider.dart';
// import 'package:nurseryapp/utils/app_colors.dart';
// import 'package:pin_code_fields/pin_code_fields.dart';
// import 'package:provider/provider.dart';
// import '../../custom_widgets/custom_painter/custom_painter.dart';
// import '../../locale/app_localizations.dart';
// import '../../networking/api_provider.dart';
// import '../../utils/app_colors.dart';
//
// class VerificationCodeScreen extends StatefulWidget {
//   @override
//   _VerificationCodeScreenState createState() => _VerificationCodeScreenState();
// }
//
// class _VerificationCodeScreenState extends State<VerificationCodeScreen>
//     with TickerProviderStateMixin {
//   AuthProvider _authProvider;
//   String _activationCode = '';
//   StreamController<ErrorAnimationType> _errorController;
//   double _width = 0;
//   TextEditingController _activationCodeController = TextEditingController();
//   bool _isLoading = false;
//
//   ApiProvider _apiProvider = ApiProvider();
//
//   AnimationController _animationController;
//
//   String get timerString {
//     Duration duration =
//         _animationController.duration * _animationController.value;
//     return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
//   }
//
//   @override
//   void initState() {
//     _errorController = StreamController<ErrorAnimationType>();
//     _animationController = AnimationController(
//       vsync: this,
//       duration: Duration(seconds: 120),
//     );
//     _animationController.reverse(
//         from: _animationController.value == 0.0
//             ? 1.0
//             : _animationController.value);
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     _errorController.close();
//     _animationController.dispose();
//     super.dispose();
//   }
//
//   Widget _buildBodyItem() {
//     return SingleChildScrollView(
//       child: Container(
//         margin: EdgeInsets.all(20.0),
//         decoration: BoxDecoration(
//             color: Colors.white, borderRadius: BorderRadius.circular(18.0)),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Container(
//               margin: EdgeInsetsDirectional.only(start: 10),
//               child: IconButton(
//                   icon: Icon(
//                     Icons.arrow_back_ios,
//                     size: 20,
//                   ),
//                   onPressed: () => Navigator.pop(context)),
//             ),
//             Container(
//                 alignment: Alignment.center,
//                 child: Image.asset(
//                   'assets/images/verification_code.png',
//                   height: 150,
//                 )),
//             Container(
//               alignment: Alignment.center,
//               margin: EdgeInsets.symmetric(vertical: 15),
//               child: Text(
//                 AppLocalizations.of(context).translate('verification_code'),
//                 style: TextStyle(
//                     color: mainAppColor,
//                     fontWeight: FontWeight.w900,
//                     fontSize: 15.0),
//               ),
//             ),
//             Container(
//                 alignment: Alignment.center,
//                 margin: EdgeInsets.symmetric(horizontal: 50),
//                 child: Text(
//                   AppLocalizations.of(context).translate(
//                       'verification_code_has_been_sent_to_your_phone'),
//                   style: TextStyle(
//                       color: Color(0xff6A6A6A),
//                       fontSize: 13,
//                       fontWeight: FontWeight.w500),
//                   textAlign: TextAlign.center,
//                 )),
//             Container(
//               margin: EdgeInsets.symmetric(vertical: 10),
//               child: Directionality(
//                 textDirection: TextDirection.ltr,
//                 child: Container(
//                   alignment: Alignment.center,
//                   margin: EdgeInsets.symmetric(horizontal: 40),
//                   child: PinCodeTextField(
//                     appContext: context,
//                     controller: _activationCodeController,
//                     keyboardType: TextInputType.number,
//                     length: 4,
//                     textStyle: TextStyle(color: nearlyRed),
//                     pinTheme: PinTheme(
//                         shape: PinCodeFieldShape.underline,
//                         selectedFillColor: mainAppColor,
//                         borderRadius: BorderRadius.circular(5),
//                         fieldHeight: 50,
//                         fieldWidth: 45,
//                         selectedColor: mainAppColor,
//                         activeColor: mainAppColor,
//                         borderWidth: 1),
//                     animationDuration: Duration(milliseconds: 300),
//                     animationType: AnimationType.scale,
//                     errorAnimationController: _errorController,
//                     onChanged: (value) {},
//                     onCompleted: (value) {},
//                   ),
//                 ),
//               ),
//             ),
//             SizedBox(
//               height: 10,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Icon(
//                   FontAwesomeIcons.undoAlt,
//                   size: 15,
//                   color: Color(0xff6A6A6A),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 Text(
//                   AppLocalizations.of(context).translate('resend'),
//                   style: TextStyle(
//                       color: Color(0xff6A6A6A),
//                       fontSize: 15,
//                       fontWeight: FontWeight.w500),
//                 )
//               ],
//             ),
//             Container(
//               margin: EdgeInsets.symmetric(
//                 vertical: 20,
//               ),
//               height: 50,
//               child: Align(
//                 alignment: FractionalOffset.center,
//                 child: AspectRatio(
//                   aspectRatio: 1.0,
//                   child: Stack(
//                     children: <Widget>[
//                       Positioned.fill(
//                         child: AnimatedBuilder(
//                           animation: _animationController,
//                           builder: (BuildContext context, Widget child) {
//                             return CustomPaint(
//                                 painter: TimerPainter(
//                                     animation: _animationController,
//                                     backgroundColor: Colors.white,
//                                     color: nearlyRed));
//                           },
//                         ),
//                       ),
//                       Align(
//                         alignment: FractionalOffset.center,
//                         child: AnimatedBuilder(
//                             animation: _animationController,
//                             builder: (BuildContext context, Widget child) {
//                               return Text(timerString,
//                                   style: TextStyle(
//                                       color: nearlyRed,
//                                       fontSize: 15,
//                                       fontWeight: FontWeight.w700));
//                             }),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             CustomButton(
//               verticalMargin: 20,
//               btnColor: nearlyGreen,
//               btnLbl: AppLocalizations.of(context).translate('confirm'),
//               onPressedFunction: () {
//                 Navigator.pushNamed(context, '/new_password_screen');
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     _authProvider = Provider.of<AuthProvider>(context);
//     return NetworkIndicator(
//         child: PageContainer(
//             child: Scaffold(
//                 body: Stack(children: [
//       BackgroundImage(),
//       _buildBodyItem(),
//     ]))));
//   }
// }
