import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:saloon/ui/auth/screen_register.dart';

import '../../custom_widgets/buttons/custom_button.dart';
import '../../custom_widgets/custom_text_form_field/custom_text_form_field.dart';

class ScreenLogin extends StatefulWidget {
  @override
  _ScreenLoginState createState() => _ScreenLoginState();
}

class _ScreenLoginState extends State<ScreenLogin> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Container(
            height: 440,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.center,
                colors: [Color(0xffE72E56), Color(0xff801D37)],
              ),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 54, right: 14, left: 14),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 54, right: 14, left: 14),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(18)),
                    child: Center(
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 60, right: 106, left: 106, bottom: 10),
                            decoration: BoxDecoration(),
                            child: Image.asset('assets/images/home1.png'),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              // ignore: deprecated_member_use
                              FlatButton(
                                height: 32,
                                minWidth: 95,
                                child: Text('تسجيل جديد'),
                                shape: Border(
                                  bottom:
                                      BorderSide(color: Colors.white, width: 3),
                                ),
                                onPressed: () {
                                  // ignore: deprecated_member_use
                                  FlatButton(
                                    shape: Border(
                                        bottom: BorderSide(
                                            color: Colors.pink, width: 3)),
                                    onPressed: () {},
                                    child: null,
                                  );
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ScreenRegister()),
                                  );
                                },
                              ),
                              // ignore: deprecated_member_use
                              FlatButton(
                                height: 34,
                                minWidth: 95,
                                child: Text('تسجيل دخول'),
                                shape: Border(
                                    bottom: BorderSide(
                                        color: Colors.pink, width: 3)),
                                onPressed: () {
                                  print('You selected all posts');
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 28,
                          ),
                          CustomTextFormField(
                            hintTxt: 'البريد الإلكتروني',
                            prefixIcon: Icon(Icons.email),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          CustomTextFormField(
                            hintTxt: 'كلمة السر',
                            prefixIcon: Icon(Icons.security),
                          ),
                          SizedBox(
                            height: 23,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // ignore: deprecated_member_use
                              FlatButton(
                                height: 32,
                                minWidth: 95,
                                child: Text('نسيت كلمة المرور؟'),
                                shape: Border(
                                    bottom: BorderSide(
                                        color: Colors.white, width: 0)),
                                onPressed: () {
                                  print('You selected all posts');
                                },
                              ),

                              // ignore: deprecated_member_use
                              FlatButton(
                                height: 32,
                                minWidth: 95,
                                child: Text('استعادة'),
                                shape: Border(
                                    bottom: BorderSide(
                                        color: Colors.white, width: 0)),
                                onPressed: () {
                                  print('You selected all posts');
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          CustomButton(
                            btnColor: Colors.pink,
                            btnLbl: 'دخول',
                            height: 50,
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          // ignore: deprecated_member_use
                          FlatButton(
                            height: 36,
                            minWidth: 26,
                            child: Text(
                              'تخطي',
                              style: TextStyle(
                                fontSize: 13,
                              ),
                            ),
                            shape: Border(
                                bottom:
                                    BorderSide(color: Colors.white, width: 0)),
                            onPressed: () {
                              print('You selected all posts');
                            },
                          ),
                          SizedBox(
                            height: 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      // ignore: deprecated_member_use
                      FlatButton(
                        height: 32,
                        minWidth: 95,
                        child: Text('أو الدخول عبر'),
                        shape: Border(
                            bottom: BorderSide(color: Colors.white, width: 0)),
                        onPressed: () {
                          print('You selected all posts');
                        },
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          MaterialButton(
                            onPressed: () {},
                            height: 54,
                            minWidth: 54,
                            color: Colors.white,
                            child: Image.asset('assets/images/g.png'),
                            padding: EdgeInsets.all(16),
                            shape: CircleBorder(),
                          ),
                          MaterialButton(
                            onPressed: () {},
                            height: 54,
                            minWidth: 54,
                            color: Colors.white,
                            child: Image.asset('assets/images/t.png'),
                            padding: EdgeInsets.all(16),
                            shape: CircleBorder(),
                          ),
                          MaterialButton(
                            onPressed: () {},
                            height: 54,
                            minWidth: 54,
                            color: Colors.white,
                            child: Image.asset('assets/images/fb.png'),
                            padding: EdgeInsets.all(16),
                            shape: CircleBorder(),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
