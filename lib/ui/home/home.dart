// import 'package:flutter/material.dart';
// import 'package:nurseryapp/custom_widgets/buttons/custom_button.dart';
// import 'package:nurseryapp/custom_widgets/carousel_slider/carousel_with_indicator.dart';
// import 'package:nurseryapp/locale/app_localizations.dart';
// import 'package:nurseryapp/utils/app_colors.dart';
// import 'package:sizer/sizer.dart';
// import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   List<String> _imgList = [
//     'assets/images/slider_img.png',
//     'assets/images/slider_img.png',
//     'assets/images/slider_img.png'
//   ];

//   Widget _buildBodyItem() {
//     return Column(
//       children: [
//         Stack(
//           children: [
//             ClipPath(
//               clipper: OvalBottomBorderClipper(),
//               child: Container(
//                 height: 20.0.h,
//                 color: mainAppColor,
//               ),
//             ),
//             Container(
//               height: 27.0.h,
//               child: CarouselWithIndicator(
//                 imgList: _imgList,
//               ),
//             )
//           ],
//         ),
//         Container(
//           alignment: Alignment.center,
//           height: 7.0.h,
//           decoration: BoxDecoration(
//               color: Color(0xffF2F2F2),
//               borderRadius: BorderRadius.circular(10.0)),
//           margin: EdgeInsets.symmetric(vertical: 3.0.h, horizontal: 4.0.w),
//           child: Text(
//             AppLocalizations.of(context).translate('our_services'),
//             style: TextStyle(
//                 color: mainAppColor,
//                 fontSize: 11.0.sp,
//                 fontWeight: FontWeight.bold),
//           ),
//         ),
//         Expanded(
//             child: ListView.builder(
//                 scrollDirection: Axis.horizontal,
//                 itemCount: 10,
//                 itemBuilder: (context, index) {
//                   return Container(
//                       margin: EdgeInsets.symmetric(horizontal: 2.0.w),
//                       child: Image.asset('assets/images/baby.png'));
//                 })),
//         CustomButton(
//           verticalMargin: 3.0.h,
//           horizontalMargin: 4.0.w,
//           btnColor: nearlyGreen,
//           btnLbl: AppLocalizations.of(context).translate('reserve_service'),
//           onPressedFunction: () {},
//         )
//       ],
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     final appBar = AppBar(
//       elevation: 0,
//       leading: IconButton(
//         icon: Icon(Icons.notifications_none_outlined),
//         color: Colors.white,
//         onPressed: () {},
//       ),
//       title: Text(
//         AppLocalizations.of(context).translate('home'),
//         style: Theme.of(context).textTheme.headline1,
//       ),
//       actions: [IconButton(icon: Icon(Icons.search), onPressed: () {})],
//       centerTitle: true,
//     );
//     return Scaffold(
//       appBar: appBar,
//       body: _buildBodyItem(),
//     );
//   }
// }
