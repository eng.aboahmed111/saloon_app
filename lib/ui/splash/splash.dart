import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:saloon/ui/auth/screen_login.dart';
import 'package:saloon/ui/auth/screen_register.dart';
import '../../providers/auth_provider.dart';
import '../../shared_preferences/shared_preferences_helper.dart';
import '../auth/screen_city.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthProvider _authProvider;

  Future initData() async {
    await Future.delayed(Duration(seconds: 3));
  }

  Future<void> _getLanguage() async {
    String currentLang =
        await SharedPreferencesHelper.getString('lang') ?? 'ar';
    _authProvider.setCurrentLanguage(currentLang);
  }

  Future<Null> _checkIsLogin() async {
    // var token = await SharedPreferencesHelper.getString("token");
    // if (token != null) {
    //   _authProvider.setAccessToken(token);
    //   Navigator.pushReplacementNamed(context, '/management_bottom_navigation');
    // } else
    Navigator.pushReplacementNamed(context, '/ScreanCity');
  }

  // Future<Null> _checkIsFirstTime() async {
  //   var _firstTime = await SharedPreferencesHelper.getBoolean("first_time") ??  true;
  //   if (_firstTime) {
  //     SharedPreferencesHelper.saveBoolean("first_time",_firstTime);
  //     Navigator.pushReplacementNamed(context, '/onboaring_screen');
  //   } else
  //     _checkIsLogin();
  // }

  Widget _buildBodyItem() {

    return AnimatedSplashScreen(
      // nextScreen: ChoosingLangScreen(),
      nextScreen: ScreanCity(),
      splash: Center(
        child: Image.asset(
          'assets/images/3.png',
          width: MediaQuery.of(context).size.width*0.7 ,
          height: 70,
        ),
      ),
      splashTransition: SplashTransition.rotationTransition,
      backgroundColor: Colors.transparent,
    );
  }

  @override
  void initState() {
    _getLanguage();
    super.initState();

    // initData().then((value) =>
    //   Navigator.pushReplacementNamed(context, '/onboaring_screen')
    // // _checkIsFirstTime()
    //     // Navigator.pushReplacementNamed(context, '/login_screen')
    //     //  _checkIsLogin()
    //     );
  }

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);

    return Scaffold(body: (_buildBodyItem()));
  }
}
