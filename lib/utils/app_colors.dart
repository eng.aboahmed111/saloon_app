import 'hex_color.dart';


final mainAppColor = HexColor('273370');
final hintColor = HexColor('D2D2D2');
final accentColor = HexColor('F7941D');
final toastWarningColor = HexColor('FF1937');
final nearlyDarkColor = HexColor('707070');
final nearlyGreen = HexColor('A6C437');
final nearlyRed =  HexColor('C53543');


