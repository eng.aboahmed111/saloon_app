//
// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:saudi_home/locale/app_localizations.dart';
//
//
//
//
// import 'package:toast/toast.dart';
//
// import 'app_colors.dart';
//
//
//
// class Commons {
//   // static const baseURL = "https://api.chucknorris.io/";
//
//   // static const tileBackgroundColor = const Color(0xFFF1F1F1);
//   // static const chuckyJokeBackgroundColor = const Color(0xFFF1F1F1);
//   // static const chuckyJokeWaveBackgroundColor = const Color(0xFFA8184B);
//   // static const gradientBackgroundColorEnd = const Color(0xFF601A36);
//   // static const gradientBackgroundColorWhite = const Color(0xFFFFFFFF);
//   // static const mainAppFontColor = const Color(0xFF4D0F29);
//   // static const appBarBackGroundColor = const Color(0xFF4D0F28);
//   // static const categoriesBackGroundColor = const Color(0xFFA8184B);
//   // static const hintColor = const Color(0xFF4D0F29);
//   // static const mainAppColor = const Color(0xFF4D0F29);
//   // static const gradientBackgroundColorStart = const Color(0xFF4D0F29);
//   // static const popupItemBackColor = const Color(0xFFDADADB);
//
//   static Widget chuckyLoader() {
//     return Center(child: SpinKitFoldingCube(
//       itemBuilder: (BuildContext context, int index) {
//         return DecoratedBox(
//           decoration: BoxDecoration(
//             color: index.isEven ? Color(0xFFFFFFFF) : Color(0xFF311433),
//           ),
//         );
//       },
//     ));
//   }
//
//   static void showError(BuildContext context, String message) {
//     showDialog(
//         context: context,
//         builder: (BuildContext context) => AlertDialog(
//               title: Text(message,style: TextStyle(
//                 color: Colors.black,
//                 fontSize: 15
//               ),),
//               backgroundColor: Colors.white,
//               shape: RoundedRectangleBorder(
//                   borderRadius: new BorderRadius.circular(15)),
//               actions: <Widget>[
//                 FlatButton(
//                   child: Text( AppLocalizations.of(context).translate('ok')),
//                   textColor: Colors.black,
//                   onPressed: () =>
//                     Navigator.of(context).pop()
//
//                 ),
//               ],
//             ));
//   }
//
//
//   static void showAlertDialog(BuildContext context,String title, String message) {
//     AlertDialog alertDialog = AlertDialog(
//       title: Text(title),
//       content: Text(message),
//     );
//     showDialog(context: context, builder: (_) => alertDialog);
//   }
//
//   static Widget chuckyLoading(String message) {
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.center,
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: <Widget>[
//         Padding(padding: EdgeInsets.all(10), child: Text(message)),
//         chuckyLoader(),
//       ],
//     );
//   }
//
//
//
//   static void  showToast(BuildContext context, {@required  String message,Color color}) {
//   return Toast.show(message, context,
//       backgroundColor: color == null ? mainAppColor: color,
//       duration: Toast.LENGTH_LONG,
//       gravity: Toast.BOTTOM);
// }
//
//   // static Future logout(BuildContext context) async {
//   //   final storage = new FlutterSecureStorage();
//   //   await storage.deleteAll();
//
//   //   Navigator.of(context).pushReplacement(
//   //       MaterialPageRoute(builder: (context) => LoginScreen()));
//   // }
//
//
// }