class Urls {
  static const String BASE_URL = "http://tagen.ibtdi.work/api/";
  static const String LOGIN_URL = BASE_URL + "login";
  static const String MENUS_URL = BASE_URL + "menus";
  static const String UPDATE_ORDER_STATUS_URL =
      BASE_URL + "update_order_status";
  static const String SCHEDULE_URL = BASE_URL + "schedule";
  static const String ADD_ITEM_TO_MENU_URL = BASE_URL + "add_item";
  static const String UPDATE_ITEM_URL = BASE_URL + "update_item/";
  static const String LIST_ITEMS_URL = BASE_URL + "list_items/";
  static const String LIST_ITEMS_IN_CERTAIN_DATE_URL = BASE_URL + "items_date";
  static const String COUNTRIES_URL = BASE_URL + "countries";
  static const String CITIES_URL = BASE_URL + "cities/";
  static const String REGIONS_URL = BASE_URL + "regions/";
  static const String REGISTER_URL = BASE_URL + "register";
  static const String FORGET_PASSSWORD_URL =
      BASE_URL + "user/auth/forgetPassword";
  static const String CHECK_ACTIVATION_CODE_URL =
      BASE_URL + "user/auth/checkCode";
  static const String RESEND_ACTIVATION_CODE_URL =
      BASE_URL + "user/auth/resendCode";
  static const String CHANGE_PASSWORD_URL =
      BASE_URL + "user/auth/changPassword";
  static const String RESET_PASSWORD_URL = BASE_URL + "user/auth/resetPassword";
  static const String NOTIFICATION_URL = BASE_URL + "user/auth/notifications";
  static const String EDIT_PROFILE_URL = BASE_URL + "user/auth/editProfile";
  static const String ALL_CAPTAINS_URL = BASE_URL + "all_captains";
  static const String ASSIGN_ORDER_UEL = BASE_URL + "assign_order";
  static const String DELETE_IMG_URL = BASE_URL + "delete_image/";
  static const String ORDER_DETAILS_URL = BASE_URL + "order_details/";
  static const String DELETE_REQUEST_URL = BASE_URL + "delete_request";
  static const String ITEM_DETAILS_URL = BASE_URL + "item/";
  static const String ORDERS_URL = BASE_URL + "kitchen_orders";
  static const String DELETE_FOOD_ITEM_URL = BASE_URL + "delete_item/";

  static const String DELETE_DAY_FOOD_ITEM_URL = BASE_URL + "delete_day_item/";

  static const String SCHEDULE_MENUS_URL = BASE_URL + "schedule_menus";
  static const String SEARCH_ITEM_URL = BASE_URL + "search_item?keyword=";
  static const String PREPARE_ITEMS_URL = BASE_URL + "prepare_items";
  static const String TOTAL_ORDERS_URL = BASE_URL + "total_orders";
  static const String FILTER_ORDERS_URL = BASE_URL + "filter_orders";
  static const String KITCHEN_PROFILE_URL = BASE_URL + "kitchen_profile";
  static const String UPDATE_ADDRESS_URL = BASE_URL + "update_address";
  static const String UPDATE_USER_URL = BASE_URL + "update_user";
  static const String UPDATE_PASSWORD_URL = BASE_URL + "update_password";
}
